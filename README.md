# Jinja2 support for Komodo Edit

This extension add support for [Jinja2](http://jinja.pocoo.org) template language to Komodo Edit/IDE.

It provides:

    - Syntax highlighting
    - Tag and filter autocompletion.

It does not support syntax checking, user-defined tags and filters.

Tested and work on Komodo Edit 10. Based on Komodo's built-in Twig support.


## How to build

Open as a project in Komodo Edit. Then look for _Build_ command under project's _Toolbox_ and run it.

## License

The software is distributed under MIT license.
